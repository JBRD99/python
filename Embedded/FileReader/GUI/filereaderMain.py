import sys
from PySide2.QtWidgets import QApplication, QMainWindow, QFileDialog
from filereaderView import Ui_MainWindow

class MainWindow(QMainWindow, Ui_MainWindow):

    fileOpened = tuple()
    fileData = str()
    fileName = str()

    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.openFileBtn()
        self.openFileMenuBtn()
        self.loadFileBtn()
        self.loadFileMenuBtn()
        self.saveFileBtn()
        self.saveFileMenuBtn()
        self.editTextMenuBtn()
        self.editTextCheckbox()        

    def openFileBtn(self):
        self.pushButton.clicked.connect(self.openFile)

    def loadFileBtn(self):
        self.pushButton_2.clicked.connect(self.loadFile)

    def saveFileBtn(self):
        self.pushButton_4.clicked.connect(self.saveFile)

    def editTextCheckbox(self):
        self.checkBox.clicked.connect(self.editText)

    def openFileMenuBtn(self):
        self.actionOpen_file.triggered.connect(self.openFile)

    def loadFileMenuBtn(self):
        self.actionLoad_file.triggered.connect(self.loadFile)

    def saveFileMenuBtn(self):
        self.actionSave_file.triggered.connect(self.saveFile)

    def editTextMenuBtn(self):
        self.actionEdit_text.triggered.connect(self.editText)

    def openFile(self):
        self.fileOpened = QFileDialog.getOpenFileName(self)
        self.fileName = self.fileOpened[0].split('/')[-1]
        if len(self.fileName) >= 1: 
            self.comboBox.addItem(self.fileName, userData = self.fileOpened[0])
            self.comboBox.setCurrentIndex(self.comboBox.count()-1)

    def loadFile(self):
        f = open(self.comboBox.currentData(), 'r')
        self.fileData = f.read()
        self.label.setText(self.comboBox.currentText())
        self.textEdit.setPlainText(self.fileData)
        f.close()

    def saveFile(self):
        f = open(self.comboBox.currentData(), 'w')
        self.fileData = f.write(self.textEdit.toPlainText())
        f.close()

    def editText(self):
        if self.checkBox.isChecked():
            self.textEdit.setReadOnly(False)
        else:
            self.textEdit.setReadOnly(True)


if (__name__ == '__main__'):

    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    
    sys.exit(app.exec_())