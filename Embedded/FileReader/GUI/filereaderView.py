# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'filereaderView.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(640, 480)
        icon = QIcon()
        icon.addFile(u"../../../../Downloads/svaret.png", QSize(), QIcon.Normal, QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.actionOpen_file = QAction(MainWindow)
        self.actionOpen_file.setObjectName(u"actionOpen_file")
        self.actionLoad_file = QAction(MainWindow)
        self.actionLoad_file.setObjectName(u"actionLoad_file")
        self.actionSave_file = QAction(MainWindow)
        self.actionSave_file.setObjectName(u"actionSave_file")
        self.actionEdit_text = QAction(MainWindow)
        self.actionEdit_text.setObjectName(u"actionEdit_text")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.pushButton = QPushButton(self.centralwidget)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(20, 60, 201, 41))
        font = QFont()
        font.setPointSize(11)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.pushButton.setFont(font)
        self.pushButton_2 = QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(20, 110, 201, 41))
        self.pushButton_2.setFont(font)
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(20, 210, 201, 21))
        font1 = QFont()
        font1.setPointSize(10)
        self.label.setFont(font1)
        self.label.setAutoFillBackground(True)
        self.label.setFrameShape(QFrame.Box)
        self.label.setAlignment(Qt.AlignCenter)
        self.checkBox = QCheckBox(self.centralwidget)
        self.checkBox.setObjectName(u"checkBox")
        self.checkBox.setGeometry(QRect(250, 20, 81, 41))
        font2 = QFont()
        font2.setPointSize(11)
        self.checkBox.setFont(font2)
        self.checkBox.setIconSize(QSize(32, 32))
        self.textEdit = QTextEdit(self.centralwidget)
        self.textEdit.setObjectName(u"textEdit")
        self.textEdit.setGeometry(QRect(250, 60, 361, 371))
        self.textEdit.setReadOnly(True)
        self.comboBox = QComboBox(self.centralwidget)
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setGeometry(QRect(340, 30, 271, 21))
        font3 = QFont()
        font3.setPointSize(9)
        self.comboBox.setFont(font3)
        self.pushButton_4 = QPushButton(self.centralwidget)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setGeometry(QRect(20, 160, 201, 41))
        self.pushButton_4.setFont(font)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 640, 22))
        self.menuMenu = QMenu(self.menubar)
        self.menuMenu.setObjectName(u"menuMenu")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuMenu.menuAction())
        self.menuMenu.addAction(self.actionOpen_file)
        self.menuMenu.addAction(self.actionLoad_file)
        self.menuMenu.addAction(self.actionSave_file)
        self.menuMenu.addSeparator()
        self.menuMenu.addAction(self.actionEdit_text)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"File Reader", None))
        self.actionOpen_file.setText(QCoreApplication.translate("MainWindow", u"Open file", None))
#if QT_CONFIG(shortcut)
        self.actionOpen_file.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+O", None))
#endif // QT_CONFIG(shortcut)
        self.actionLoad_file.setText(QCoreApplication.translate("MainWindow", u"Load file", None))
#if QT_CONFIG(shortcut)
        self.actionLoad_file.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+L", None))
#endif // QT_CONFIG(shortcut)
        self.actionSave_file.setText(QCoreApplication.translate("MainWindow", u"Save file", None))
#if QT_CONFIG(shortcut)
        self.actionSave_file.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionEdit_text.setText(QCoreApplication.translate("MainWindow", u"Edit text", None))
#if QT_CONFIG(shortcut)
        self.actionEdit_text.setShortcut(QCoreApplication.translate("MainWindow", u"Ctrl+E", None))
#endif // QT_CONFIG(shortcut)
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"Open file", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"Load file", None))
        self.label.setText("")
        self.checkBox.setText(QCoreApplication.translate("MainWindow", u"Edit text", None))
        self.pushButton_4.setText(QCoreApplication.translate("MainWindow", u"Save file", None))
        self.menuMenu.setTitle(QCoreApplication.translate("MainWindow", u"Menu", None))
    # retranslateUi

