import sys

class FileManipulator:

	def write(self, data):
		try:
			with open(self.filepath, 'a') as file:
				file.write(data + '\n')
			file.close()

			return True

		except Exception as e:
			print(e)
			return False


	def read(self):
		try:
			with open(self.filepath, 'r') as file:
				for line in file:
					print(line)
			file.close()

		except:

			return False


	def set_file_path(self, new_path):
		try:
			if sys.platform.startswith('win'):
				self.filepath = new_path
			elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
				self.filepath = '.' + new_path
			

			return True

		except:

			return False
