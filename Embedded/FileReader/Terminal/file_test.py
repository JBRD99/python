import pytest
from file_manipulator import FileManipulator

@pytest.fixture()
def fmp():
	return FileManipulator("C:\\Users\\Jakob\\Desktop\\GitLab\\python\\Embedded\\Files\\example1.txt")

def test_CanInstantiateClass(fmp):
	assert fmp != None

def test_CanWriteToFile(fmp):
	assert fmp.write({"0013A20041B8AB74": "EP_1.0"})

def test_CanReadFile(fmp):
	assert fmp.read

def test_CanChangeFilePath(fmp):
	assert fmp.change_file_path("C:\\Users\\Jakob\\Desktop\\GitLab\\python\\Embedded\\Files\\example2.txt")