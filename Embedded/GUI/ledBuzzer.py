from PySide2.QtWidgets import QMainWindow, QApplication, QPushButton, QComboBox, QCheckBox, QLabel
from gpiozero import *
from gpiozero.tones import Tone
from time import sleep
import sys 
  
ledRed = LED(16)
reedSwitch = Button(12, True)
passiveBuzzer = TonalBuzzer(26)

class Window(QMainWindow): 

    def __init__(self): 
        super().__init__() 
  
        # setting title 
        self.setWindowTitle("LED Buzzer GUI") 
  
        # setting geometry 
        self.setGeometry(100, 100, 400, 400) 
  
        # calling method 
        self.UiComponents() 
  
        # showing all the widgets 
        self.show() 
  
    # method for widgets 
    def UiComponents(self): 
  
        # creating a push button 
        ledBtn = QPushButton("Blink", self) 
        buzzerBtn = QPushButton("Buzz", self)
        buzzerNotes = QComboBox(self)
        buzzerNotes.addItems(["C4","D4","E4","F4"])
        blinkTwice = QCheckBox(self)
        checkBoxLabel = QLabel(self)
        checkBoxLabel.setText("Enable for 5 blinks")
  
        # setting geometry of button 
        ledBtn.setGeometry(100, 120, 200, 60) 
        blinkTwice.setGeometry(290,90,20, 20)
        buzzerBtn.setGeometry(100, 200, 200, 60) 
        buzzerNotes.setGeometry(100,280,200,60)
        checkBoxLabel.setGeometry(110,80,170,40)

        # adding action to a button 
        ledBtn.clicked.connect(lambda: self.blinkLed(blinkTwice.isChecked())) 
        buzzerBtn.clicked.connect(lambda: self.buzzBuzzer(buzzerNotes.currentText()))


    # action method 
    def blinkLed(self, isChecked): 
        if isChecked == True:  
            ledRed.blink(0.1,0.1,5)
        else:
            ledRed.blink(0.1,0.1,1)

    def buzzBuzzer(self, note):
        passiveBuzzer.play(Tone(note))
        sleep(0.2)
        passiveBuzzer.stop()
  
# create pyqt5 app 
App = QApplication(sys.argv) 
  
# create the instance of our Window 
window = Window() 
  
# start the app 
sys.exit(App.exec_()) 