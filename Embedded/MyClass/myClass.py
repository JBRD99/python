import sys

class myClass:

    def __init__(self, animal):
        self.animal = animal

        if self.animal == '1':
            self.cat()
        elif self.animal == '2':
            self.dog()
        elif self.animal == '3':
            self.pig()
        else:
            sys.exit(0)

    def cat(self):
        print("\nmeow")

    def dog(self):
        print("\nwoof")

    def pig(self):
        print("\noink")