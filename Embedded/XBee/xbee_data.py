try:
    import Adafruit_DHT
except ImportError:
    pass
import json

class XBeeData:

    def __init__(self, contr):
        self.contr = contr

    ## ---------------------------------------------------------------------------------------------------
    ## Reads data from DHT11 sensor
    ## ---------------------------------------------------------------------------------------------------
    def get_dht11_sensor_data(self):
        try:
            humidity, temperature = Adafruit_DHT.read(Adafruit_DHT.DHT11, 17)
            if humidity is not None and temperature is not None:
                data = "Temperature={0:0.1f}C Humidity={1:0.1f}%".format(temperature, humidity)
                self.contr.tools.write_to_console_output("Read sensor data: " + data)
                return data
            else:
                return "N/A"

        except Exception as e:
            self.contr.tools.write_to_console_output(str(e))
            return "Temperature={0:0.1f}C Humidity={1:0.1f}%".format(0, 0)

    ## ---------------------------------------------------------------------------------------------------
    ## Saves data from DHT11 sensor
    ## ---------------------------------------------------------------------------------------------------
    def save_dht11_sensor_data(self, msg):
        try:
            data = msg.split('=')
            temp = data[1][0:4]
            humi = data[2][0:4]
            self.contr.tools.set_file_path("./Files/data.json")
            with open(self.contr.tools.filepath, 'r') as json_file:
                json_data = json.load(json_file)
                json_data["sensor_data"].append({
                    'Temperature': temp,
                    'Humidity': humi
                    })
            with open(self.contr.tools.filepath, 'w') as json_file:
                json.dump(json_data, json_file, ensure_ascii=False, indent=4)
            
            return True

        except Exception as e:
            self.contr.tools.write_to_console_output(str(e))
            return False