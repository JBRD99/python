from datetime import datetime
import os
from digi.xbee.models.address import XBee64BitAddress
from digi.xbee.exception import TransmitException
from xbee_data import XBeeData

class XBeeCommunication():

    def __init__(self, contr):
        self.contr = contr
        self.data = XBeeData(self.contr)


    ## ---------------------------------------------------------------------------------------------------
    ## Adds callback function to local device whenever data is received
    ## ---------------------------------------------------------------------------------------------------
    def listen_for_data(self):
        try:
            self.contr.device.add_data_received_callback(self.receive_data)
            self.contr.tools.write_to_console_output("Listening for data...")
            self.contr.btn_request_data.setEnabled(True)

            return True

        except Exception as e:
            self.contr.tools.write_to_console_output(e)
            self.contr.btn_request_data.setEnabled(False)

            return False


    ## ---------------------------------------------------------------------------------------------------
    ## Checks if the received message is a request for data or sensor data
    ## ---------------------------------------------------------------------------------------------------
    def receive_data(self, xbee_message):
        ## Displaying message to console output
        mac_addr = str(xbee_message.remote_device.get_64bit_addr())
        msg_data = xbee_message.data.decode("utf8")
        if msg_data == "REQ_FOR_DATA":  ## Check if message was a request for data
            self.send_broadcast_data_to_devices()
        elif msg_data != "N/A":
            self.data.save_dht11_sensor_data(msg_data)    ## Write sensor data to json file
        self.contr.tools.write_to_console_output(f"{datetime.now().strftime('%H:%M:%S')} Received data from {mac_addr}: {msg_data}")


    ## ---------------------------------------------------------------------------------------------------
    ## Sends data to given device selected from UI device table
    ## ---------------------------------------------------------------------------------------------------
    def send_data_to_device(self):
        msg = self.contr.txtbox_write_msg.toPlainText()
        if msg == '':
            self.contr.tools.write_to_console_output("Message cannot be blank")
            return

        selected_device = self.contr.table_remote_devices.item(self.contr.table_remote_devices.currentRow(),1)
        if selected_device == None:
            self.contr.tools.write_to_console_output("No device was selected")
            return

        mac_address = XBee64BitAddress.from_hex_string(selected_device.text())
        try:
            self.contr.device._send_data_64(mac_address, msg)
            self.contr.tools.write_to_console_output(f"Message: \"{msg}\" was succesfully sent to {mac_address}")
        except TransmitException as te:
            self.contr.tools.write_to_console_output(f"Could not send message to {mac_address}: " + str(te))

        del msg, mac_address


    ## ---------------------------------------------------------------------------------------------------
    ## Sends sensor data to all devices on same network as local device
    ## ---------------------------------------------------------------------------------------------------
    def send_broadcast_data_to_devices(self):
        data = self.data.get_dht11_sensor_data()
        self.contr.device.send_data_broadcast(data)
        self.contr.tools.write_to_console_output(f"Message: \"{data}\" was succesfully sent to every device listening")


    ## ---------------------------------------------------------------------------------------------------
    ## Sends request for sensor data to all devices on same network as local device
    ## ---------------------------------------------------------------------------------------------------
    def receive_broadcast_data_from_devices(self):
        self.contr.tools.write_to_console_output("Requesting data from devices on network")
        self.contr.device.send_data_broadcast("REQ_FOR_DATA")