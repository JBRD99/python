from xbee_discover import XBeeDiscover
from xbee_communication import XBeeCommunication
from xbee_tools import Tools
from xbee_ui import Ui_MainWindow

from PyQt5.QtWidgets import QMainWindow, QApplication
from digi.xbee.devices import XBeeDevice
from sys import exit as sys_exit
from sys import argv as sys_argv

class XBeeController(QMainWindow, Ui_MainWindow): 

    def __init__(self):
        pass

    ## ---------------------------------------------------------------------------------------------------
    ## Enables other modules used for the system
    ## ---------------------------------------------------------------------------------------------------
    def enable_discover(self):
        self.discover = XBeeDiscover(self)


    def enable_communication(self):
        self.comms = XBeeCommunication(self)


    def enable_tools(self):
        self.tools = Tools(self)

    ## ---------------------------------------------------------------------------------------------------
    ## Calls all necessarily functions for UI to work
    ## ---------------------------------------------------------------------------------------------------
    def start_ui(self):  
        app = QApplication(sys_argv)
        app.setStyle('Fusion') 
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.clicked_refresh_com_ports_btn()
        self.clicked_create_local_device_btn()
        self.clicked_open_local_device_btn()
        self.clicked_listen_for_data_btn()
        self.clicked_send_sensor_data_btn()
        self.clicked_search_for_devices_btn()
        self.clicked_load_devices_from_db_btn()
        self.clicked_send_message_btn() 
        self.clicked_request_data_btn()
        ## Retrieves all active COM Ports before showing UI, for better UX
        self.tools.get_com_ports()
        self.show()
        sys_exit(app.exec_())

    ## ---------------------------------------------------------------------------------------------------
    ## Instantiates local device with COM port and baudrate chosen from UI
    ## ---------------------------------------------------------------------------------------------------
    def create_local_device(self):
        try:
            port = self.dd_com_port.currentText()
            baud_rate = self.dd_baud_rate.currentText()
            self.tools.write_to_console_output(f"Using port: {port} and baud rate: {baud_rate} to instantiate XBeeDevice")
            # Instantiate a local XBee object
            self.device = XBeeDevice(port, baud_rate)
            self.tools.write_to_console_output(f"Instantiation was succesful")
            
            self.btn_open_local_device.setEnabled(True)
            return True

        except Exception as e:
            print(e)
            self.btn_open_local_device.setEnabled(False) 
            return False

    ## ---------------------------------------------------------------------------------------------------
    ## Open local device and calls function to configure UI based on role
    ## ---------------------------------------------------------------------------------------------------
    def open_local_device(self):
        try:         
            self.tools.write_to_console_output(f"Opening local device")
            self.device.open()
            self.tools.write_to_console_output(f"Local device was opened succesfully: {self.device.get_64bit_addr()} - {self.device.get_node_id()}")
            self.tools.write_to_console_output(f"Local device role is: {self.device.get_role().description}")
            if self.tools.configure_ui_based_on_role(self.device) == 2:
                self.comms.listen_for_data()
            self.tools.ui_enable_communication()
            return True

        except Exception as e:
            print(e)
            self.tools.ui_disable_communication()
            return False

    ## -------------------------------------------------------------------------------------------
    ## Passes reference to a given function that are to be called when clicked on the given button
    ## -------------------------------------------------------------------------------------------
    def clicked_refresh_com_ports_btn(self):
        self.btn_refresh_com_ports.clicked.connect(lambda: self.tools.get_com_ports())


    def clicked_create_local_device_btn(self):
        self.btn_create_local_device.clicked.connect(lambda: self.create_local_device())


    def clicked_open_local_device_btn(self):
        self.btn_open_local_device.clicked.connect(lambda: self.open_local_device())


    def clicked_listen_for_data_btn(self):
        self.btn_listen_for_data.clicked.connect(lambda: self.comms.listen_for_data())


    def clicked_send_sensor_data_btn(self):
        self.btn_send_sensor_data.clicked.connect(lambda: self.comms.send_broadcast_data_to_devices())


    def clicked_search_for_devices_btn(self):
        self.btn_search_remote_devices.clicked.connect(lambda: self.discover.discover_devices())


    def clicked_load_devices_from_db_btn(self):
        self.btn_load_devices_db.clicked.connect(lambda: self.discover.load_from_local_db())


    def clicked_send_message_btn(self):
        self.btn_send_message.clicked.connect(lambda: self.comms.send_data_to_device())


    def clicked_request_data_btn(self):
        self.btn_request_data.clicked.connect(lambda: self.comms.receive_broadcast_data_from_devices())

if __name__ == "__main__":

    controller = XBeeController()
    ## Enable all functionality
    controller.enable_tools()
    controller.enable_discover()
    controller.enable_communication()
    ## Start and show UI
    controller.start_ui()