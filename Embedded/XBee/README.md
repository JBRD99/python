# XBee Code

## Network diagram
![](xbee_network_diagram.jpg)

## Class diagram
![](xbee_class_diagram.jpg)

## Flowchart
![](xbee_flowchart.jpg)
