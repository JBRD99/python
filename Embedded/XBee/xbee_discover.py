from time import sleep
from digi.xbee.models.status import NetworkDiscoveryStatus
from PyQt5 import QtWidgets
import json

class XBeeDiscover:

    def __init__(self, contr):
        self.contr = contr

    ## ---------------------------------------------------------------------------------------------------
    ## When device is discovered it gets added to the UI device table and local json database
    ## ---------------------------------------------------------------------------------------------------
    def discover_callback(self, remote):
        self.contr.tools.write_to_console_output(f"Remote device was succesfully discovered: {remote}") 
        remote = {"node_id":str(remote.get_node_id()),"mac_addr":str(remote.get_64bit_addr())}
        self.add_remote_device_to_table(remote)
        self.add_remote_device_to_db(remote)
        return True


    ## ---------------------------------------------------------------------------------------------------
    ## When the discovery process completes it checks the status for either success or a time out error
    ## ---------------------------------------------------------------------------------------------------
    def discovery_process_callback(self, status):
        if status == NetworkDiscoveryStatus.SUCCESS:
            self.contr.tools.write_to_console_output(f"Discovery of remote devices was completed")
        elif status == NetworkDiscoveryStatus.ERROR_READ_TIMEOUT:
            self.contr.tools.write_to_console_output(f"Discovery of remote devices timed out")
        return True


    ## ---------------------------------------------------------------------------------------------------
    ## Discover devices on the same network as the local device
    ## ---------------------------------------------------------------------------------------------------
    def discover_devices(self):
        self.contr.tools.write_to_console_output("Attempting to discover remote devices...")
        # Get the XBee network object from the local XBee.
        xnet = self.contr.device.get_network()

        # Clean up if method has been called previously 
        self.contr.tools.clean_up_table()
        try:
            xnet.del_device_discovered_callback(self.discover_callback)
            xnet.del_discovery_process_finished_callback(self.discovery_process_callback)
        except:
            self.contr.tools.write_to_console_output("Callback discover functions has not been added")

        # Add the device discovered callback.
        xnet.add_device_discovered_callback(self.discover_callback)

        # Add the discovery process finished callback.
        xnet.add_discovery_process_finished_callback(self.discovery_process_callback)

        # Start the discovery process and wait for it to be over.
        xnet.start_discovery_process()
        while xnet.is_discovery_running():
            sleep(0.5)

        nodes = xnet.get_devices()

        if nodes != []:
            self.contr.tools.write_to_console_output(f"{len(nodes)} remote device(s) were discovered")
        else:
            self.contr.tools.write_to_console_output("No remote devices were discovered")

        del nodes, xnet
        return True


    ## ---------------------------------------------------------------------------------------------------
    ## Adds device to UI device table
    ## ---------------------------------------------------------------------------------------------------
    def add_remote_device_to_table(self, device):
        self.contr.table_remote_devices.setRowCount(self.contr.table_remote_devices.rowCount() + 1)

        device_node_id = QtWidgets.QTableWidgetItem(device.get('node_id'))
        device_mac_address = QtWidgets.QTableWidgetItem(device.get('mac_addr'))

        self.contr.table_remote_devices.setItem(self.contr.table_remote_devices.rowCount() - 1, 0, device_node_id)        
        self.contr.table_remote_devices.setItem(self.contr.table_remote_devices.rowCount() - 1, 1, device_mac_address)

        self.contr.tools.write_to_console_output(f"Loaded {device_node_id.text()} into device table")


    ## ---------------------------------------------------------------------------------------------------
    ## Adds device to local json database
    ## ---------------------------------------------------------------------------------------------------
    def add_remote_device_to_db(self, device):
        node_id = device.get('node_id')
        mac_addr = device.get('mac_addr')

        self.contr.tools.set_file_path("./Files/xbee_devices.json")
        with open(self.contr.tools.filepath) as json_file:
            json_data = json.load(json_file)
        if mac_addr not in json_data["xbee_devices"].__str__():
            json_data["xbee_devices"].append({
                'node_id': node_id,
                'mac_addr': mac_addr
                })
            with open(self.contr.tools.filepath, 'w') as json_file:
                json.dump(json_data, json_file, ensure_ascii=False, indent=4)
            self.contr.tools.write_to_console_output(f"Added remote device: {mac_addr} to local database")


    ## ---------------------------------------------------------------------------------------------------
    ## Loads all devices from local json database
    ## ---------------------------------------------------------------------------------------------------
    def load_from_local_db(self):
        try:
            self.contr.tools.clean_up_table()
            self.contr.tools.set_file_path("./Files/xbee_devices.json")
            with open(self.contr.tools.filepath) as json_file:
                json_data = json.load(json_file)
                if len(json_data["xbee_devices"]) >= 1:
                    for device in json_data["xbee_devices"]:
                        self.add_remote_device_to_table(device)
                    return True
                else:
                    self.contr.tools.write_to_console_output(f"Local database is empty!")
                    return False

        except Exception as e:
            self.contr.tools.write_to_console_output(e)
            return False