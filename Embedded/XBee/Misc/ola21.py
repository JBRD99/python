from digi.xbee.devices import XBeeDevice
from gpiozero import LED, Button
import time
import sys

try:

    yellowLed = LED(21)
    smolButon = Button(16)

    device = XBeeDevice("/dev/ttyS0", 9600)
    device.open()

    xnet = device.get_network()

    xnet.start_discovery_process()
    while xnet.is_discovery_running():
        time.sleep(0.5)
    
    nodes = xnet.get_devices()
    for node in nodes:
        remoteNI = node.get_node_id()
        remoteMAC = node.get_64bit_addr()
        print("Discovered node:\nNI:",remoteNI,"\nMAC:",remoteMAC)
        remote_device = xnet.discover_device(remoteNI)
        break

    while True:

        if smolButon.is_pressed:
            print(device.send_data(remote_device,"Button is pressed!"))

        xbee_msg = device.read_data_from(remote_device)
        if xbee_msg != None:
            msg = xbee_msg.data.decode()
            print("Message received:", msg)
            if msg == "LED ON":
                yellowLed.on()
            elif msg == "LED OFF":
                yellowLed.off()
        time.sleep(0.2)
    

except KeyboardInterrupt:
    device.close()
    yellowLed.off()
    sys.exit(0)
