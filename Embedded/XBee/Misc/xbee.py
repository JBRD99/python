import time
import serial
import RPi.GPIO as GPIO

uart = serial.Serial("/dev/ttyS0", baudrate=9600, parity = serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS, timeout = 0.050)

while True:

    print(uart.read(10).decode('ascii'))

    uart.write(str.encode('a'))
    time.sleep(0.1)