from digi.xbee.devices import XBeeDevice
from ADCDevice import *
import RPi.GPIO as GPIO
import time
import sys

try:

    adc = ADS7830()
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.OUT)

    device = XBeeDevice("/dev/ttyS0", 9600)
    device.open()

    pwm = GPIO.PWM(17,1) 
    pwm.start(0)

    xnet = device.get_network()

    xnet.start_discovery_process()
    while xnet.is_discovery_running():
        time.sleep(0.5)
    
    nodes = xnet.get_devices()
    for node in nodes:
        remoteNI = node.get_node_id()
        remoteMAC = node.get_64bit_addr()
        print("Discovered node:\nNI:",remoteNI,"\nMAC:",remoteMAC)
        remote_device = xnet.discover_device(remoteNI)
        break

    while True:

        xbee_msg = device.read_data_from(remote_device)
        value = adc.analogRead(0)
        # resistance = value * 39.21
        dutycycle = int(value*100/255)
        print ("ADC bit value: %d, Duty cycle: %d"%(value, dutycycle))
        device.send_data(remote_device, 'ADC bit value: %d, Duty cycle: %d'%(value, dutycycle))
        pwm.ChangeDutyCycle(dutycycle)
        if xbee_msg != None:
            msg = xbee_msg.data.decode()
            print("Message received:", msg)
            if msg.startswith("pwm"):
                # dutycycle = float(msg.split(',')[1])
                freq = float(msg.split(',')[1])
                # pwm.ChangeDutyCycle(dutycycle)
                pwm.ChangeFrequency(freq)
                device.send_data(remote_device, f"PWM: Duty cycle is: {dutycycle}% | Frequency has been set to: {freq}Hz")
        time.sleep(0.5)
    
except KeyboardInterrupt:
    GPIO.cleanup()
    device.close()
    sys.exit(0)
