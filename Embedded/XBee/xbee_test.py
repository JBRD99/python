import pytest
from xbee_controller import XBeeController as contr

port = "COM3"
baudrate = 9600

@pytest.fixture()
def controller():
    controller = contr() 
    return controller

@pytest.fixture()
def controller_with_discover(controller):
    controller.enable_discover()
    return controller

@pytest.fixture()
def controller_with_communication(controller):
    controller.enable_communication()
    return controller

@pytest.fixture()
def controller_with_tools(controller):
    controller.enable_tools()
    return controller

## Controller ##
def test_InstantiateClass(controller):
    assert controller != None

def test_EnableDiscover(controller_with_discover):
    assert controller_with_discover.discover != None

def test_EnableCommunication(controller_with_communication):
    assert controller_with_communication.comms != None

def test_EnableTools(controller_with_tools):
    assert controller_with_tools.tools != None

def test_CanOpenLocalDevice(controller):
    assert controller.open_local_device


## Discover  ##
def test_CanSetDiscoverCallback(controller_with_discover):
    assert controller_with_discover.discover.discover_callback

def test_CanSetDiscoveryProcessCallback(controller_with_discover):
    assert controller_with_discover.discover.discovery_process_callback
