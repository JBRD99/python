import glob
import serial
import sys

class Tools:

    def __init__(self, contr):
        self.contr = contr


    ## ---------------------------------------------------------------------------------------------------
    ## Checks if OS is Windows or Linux/GNU and alters the file path respectively to the OS path standards
    ## ---------------------------------------------------------------------------------------------------
    def set_file_path(self, new_path):
        try:
            if sys.platform.startswith('win'):
                self.filepath = new_path
            elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
                self.filepath = '.' + new_path
            return True

        except Exception as e:
            self.write_to_console_output(e)
            return False

    ## ---------------------------------------------------------------------------------------------------
    ## Configures the UI based on the role of the XBee Device
    ## ---------------------------------------------------------------------------------------------------
    def configure_ui_based_on_role(self, device):
        '''
        0 == COORDINATOR
        1 == ROUTER 
        2 == END_DEVICE 
        3 == UNKNOWN
        '''
        device_role_id = device.get_role().id
        if device_role_id == 0:
            self.coordinator_ui()
        elif device_role_id == 2:
            self.end_device_ui()
        return device_role_id

    ## ---------------------------------------------------------------------------------------------------
    ## Enables communication functionality 
    ## ---------------------------------------------------------------------------------------------------
    def ui_enable_communication(self):
        self.contr.btn_listen_for_data.setEnabled(True)
        self.contr.btn_search_remote_devices.setEnabled(True)
        self.contr.btn_send_message.setEnabled(True)


    ## ---------------------------------------------------------------------------------------------------
    ## Disables communication functionality 
    ## ---------------------------------------------------------------------------------------------------
    def ui_disable_communication(self):
        self.contr.btn_listen_for_data.setEnabled(False)
        self.contr.btn_search_remote_devices.setEnabled(False)
        self.contr.btn_send_message.setEnabled(False)
        self.contr.btn_send_sensor_data.setEnabled(False)


    ## ---------------------------------------------------------------------------------------------------
    ## Configures the UI to include the functionality relavent to a device with the 'Coordinator' role
    ## ---------------------------------------------------------------------------------------------------           
    def coordinator_ui(self):
        self.contr.setWindowTitle("XBee Controller - Coordinator")
        self.contr.lbl_write_msg_help_text.show()
        self.contr.txtbox_write_msg.show()
        self.contr.btn_send_message.show()
        self.contr.btn_request_data.show()
        self.contr.btn_listen_for_data.show()
        self.contr.line_2.show()

        self.contr.btn_send_sensor_data.hide()


    ## ---------------------------------------------------------------------------------------------------
    ## Configures the UI to include the functionality relavent to a device with the 'End device' role
    ## ---------------------------------------------------------------------------------------------------  
    def end_device_ui(self):
        self.contr.setWindowTitle("XBee Controller - End device")
        self.contr.btn_send_sensor_data.show()
        if "Adafruit_DHT" in sys.modules:
            self.contr.btn_send_sensor_data.setEnabled(True)
        else:
            self.contr.btn_send_sensor_data.setEnabled(False)

        self.contr.lbl_write_msg_help_text.hide()
        self.contr.txtbox_write_msg.hide()
        self.contr.btn_send_message.hide()
        self.contr.btn_request_data.hide()
        self.contr.btn_listen_for_data.hide()
        self.contr.line_2.hide()


    ## ---------------------------------------------------------------------------------------------------
    ## Writes to UI console output and prints message to terminal
    ## ---------------------------------------------------------------------------------------------------  
    def write_to_console_output(self, msg):
        print(msg)
        self.contr.lbl_console_output.addItem(msg)
        self.contr.lbl_console_output.scrollToBottom()


    ## ---------------------------------------------------------------------------------------------------
    ## Retreives all active COM ports by checking if a serial connection can be instantiated with the given COM port
    ## ---------------------------------------------------------------------------------------------------     
    def get_com_ports(self):
        self.contr.dd_com_port.clear()
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(10)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        for port in ports:
            try:
                self.write_to_console_output(f"Checking if: {port} is active")
                s = serial.Serial(port)
                s.close()
                self.write_to_console_output(f"{port} is active")
                self.contr.dd_com_port.addItem(port)
                self.write_to_console_output(f"Added {port} to list of active COM ports")
            except (OSError, serial.SerialException):
                pass
        
        if self.contr.dd_com_port.count() >= 1:
            self.contr.btn_create_local_device.setEnabled(True)
        else:
            self.contr.btn_create_local_device.setEnabled(False)
            self.write_to_console_output("Could not find any active ports!")

    ## ---------------------------------------------------------------------------------------------------
    ## Deletes all rows in UI device table
    ## ---------------------------------------------------------------------------------------------------    
    def clean_up_table(self):
        if self.contr.table_remote_devices.rowCount() >= 1:
            for r in range(self.contr.table_remote_devices.rowCount()):
                self.contr.table_remote_devices.removeRow(r)
            self.contr.table_remote_devices.setRowCount(0)