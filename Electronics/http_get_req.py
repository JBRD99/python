import network
import socket

sta_if = network.WLAN(network.STA_IF)
if not sta_if.isconnected ():
    print ('connecting to network ... ')
sta_if.active(True)

sta_if.connect ('JC','j10112020c')
while not sta_if.isconnected():
    pass
print('network config:',sta_if.ifconfig())

def http_get(url):
    addr = socket.getaddrinfo(url, 80)[0][-1]
    sock = socket.socket()
    sock.connect(addr)
    sock.send(b"GET /HTTP/1.0\r\n\r\n")
    data = sock.recv(2048)
    print (str(data, "utf8"))
    sock.close()

http_get(input("Enter a valid url:\n>"))

print ('disconnecting from the network ... ')
sta_if.disconnect()

print('Network status:',sta_if.isconnected())

