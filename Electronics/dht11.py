from machine import Pin, I2C
import dht
from machine import Pin
import ssd1306
from time import sleep

# ESP8266 Pin assignment
i2c = I2C(scl=Pin(5), sda=Pin(4))
wakeUpBtn = Pin(13, Pin.IN)
statusLed = Pin(14, Pin.OUT)

oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

def dht11():
    d = dht.DHT11(Pin(0))
    d.measure()
    t = d.temperature()
    h = d.humidity()
    return t, h

# if __name__ == "__main__":
#     while True:
#         if wakeUpBtn == True:
#             statusLed.value(1)
#             # temp, humd = dht11()
#             temp, humd = 10, 10
#             print(temp, humd)
#             oled.fill(0)
#             oled.text("Temperature: %dc" %(temp), 0, 0)
#             oled.text("Humidity: %d%%" %(humd), 0, 17)             
#             oled.show()
#             sleep(2)
#             statusLed.value(0)

temp, humd = 10, 10
print(temp, humd)
oled.fill(0)
oled.text("Temperature: %dc" %(temp), 0, 0)
oled.text("Humidity: %d%%" %(humd), 0, 17)             
oled.show()
sleep(2)
