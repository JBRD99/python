import socket
import network

sta_if = network.WLAN(network.STA_IF)
if not sta_if.isconnected ():
    print ('connecting to network ... ')
sta_if.active(True)

sta_if.connect ('JC','j10112020c')
while not sta_if.isconnected():
    pass
print('network config:',sta_if.ifconfig())

message = "Hello from the server!"

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_addr = (sta_if.ifconfig()[0], 10000)
sock.bind(server_addr)
print("Binded to", server_addr)
sock.listen(1)
conn, client_address = sock.accept()

data = conn.recvfrom(1024)
print ("Received message:", data)

conn.sendall(message)
print ("Sent", message, "to client!")

conn.close()
print("Closed connection")
